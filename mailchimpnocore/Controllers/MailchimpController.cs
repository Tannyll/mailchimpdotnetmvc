﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using MailChimp.Helper;
using mailchimp.Models;

using MailChimp.Net.Core;
using MailChimp.Net.Models;
using MailChimp;

namespace mailchimpnocore.Controllers
{
    public class MailchimpController : Controller
    {
       static  MailChimpManager manager = new MailChimpManager("4886ea5bb9f4255f7c0ae9f8fec52bbf-us20");
       static  MailChimp.Net.MailChimpManager managerNet = new MailChimp.Net.MailChimpManager("4886ea5bb9f4255f7c0ae9f8fec52bbf-us20");
        
      // GET: Mailchimp
        public  ActionResult Index()
        {
            return View();
        }

        public ActionResult Manage()
        {
            return View();
        }

        public async Task<ActionResult> SentCampaigns()
        {
        
          MailChimp.Net.MailChimpManager _manager = new MailChimp.Net.MailChimpManager("4886ea5bb9f4255f7c0ae9f8fec52bbf-us20");

        var options = new CampaignRequest
            {
                ListId = "4b1c95ee2b",
                Status = CampaignStatus.Sent,
                SortOrder = CampaignSortOrder.DESC,
                Limit = 10
            };

            try
            {
                var model = await _manager.Campaigns.GetAllAsync(options);
                return View(model);
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable,e.Message);
            }

        }


    }
}