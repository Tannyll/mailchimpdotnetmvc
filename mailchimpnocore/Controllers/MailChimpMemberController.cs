﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using MailChimp;
using MailChimp.Errors;
using MailChimp.Helper;
using mailchimp.Models;
using MailChimp.Net.Core;
using MailChimp.Net.Models;
using MailChimpManager = MailChimp.Net.MailChimpManager;

namespace mailchimpnocore.Controllers
{
    public class MailChimpMemberController : Controller
    {
        private static string mailChimpApiKey = ConfigurationManager.AppSettings["MailChimpApiKey"];
        private static readonly MailChimpManager manager = new MailChimpManager(mailChimpApiKey);

        // GET: MailChimpMember
        public async Task<ActionResult> Index()
        {
            try
            {
                ViewBag.ListId = "4b1c95ee2b";
                var model = await manager.Members.GetAllAsync("4b1c95ee2b", new MemberRequest
                {
                    Limit = 100,
                    Status = Status.Subscribed
                });
                return View(model);
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, e.Message);
            }
        }

        public async Task<ActionResult> Detail(string id = "serdar@emirci.com")
        {
            try
            {
                var model = await manager.Members.GetAsync("4b1c95ee2b", id);
                return View(model);
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, e.Message);
            }
        }

        public ActionResult CreateFull()
        {
            var model = new MailchimpModel();
            model.Message = "";

            return View(model);
        }

       
        [HttpPost]
        public async Task<ActionResult> Create(FormCollection collection)
        {
            var model = new MailchimpModel();
            model.Message = "";

            var fname = collection["fname"];
            var lname = collection["lname"];
            var email = collection["email"];

            var member = new Member
            {
                EmailAddress = email,
                Status = Status.Subscribed,
                EmailType = "html",
                IpSignup = Request.UserHostAddress,
                MergeFields = new Dictionary<string, object>
                {
                    {"FNAME", fname},
                    {"LNAME", lname}
                },
                TimestampSignup = DateTime.UtcNow.ToString("s")
            };

            try
            {
                
                var result = await manager.Members.AddOrUpdateAsync("4b1c95ee2b", member);
                model.Message = "Teşekkürler, Üyeliğiniz başarıyla alındı!";
                return RedirectToAction("Detail", new {id = result.Id});
            }
            catch (MailChimpException mce)
            {
                model.Message = mce.Message;
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, e.Message);
            }
        }


        public async Task<ActionResult> Update(string id = "s@emirci.com")
        {
            var member = new Member
            {
                MergeFields = new Dictionary<string, object>
                {
                    {"FNAME", "Serdar"},
                    {"LNAME", "EMIRCI"}
                }
            };
            try
            {
                var result = await manager.Members.AddOrUpdateAsync("4b1c95ee2b", member);
                return RedirectToAction("Detail", new {id = result.Id});
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, e.Message);
            }
        }

        public async Task<ActionResult> Delete(string id = "s@emirci.com")
        {
            try
            {
                await manager.Members.DeleteAsync("4b1c95ee2b", id);
                return RedirectToAction("Index");
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, e.Message);
            }
        }

        public ActionResult Add()
        {
            var model = new MailchimpModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddSubscribe(FormCollection formCollection)
        {
            var email = formCollection["email"];
            var model = new MailchimpModel();
            model.Message = "";

            var mailChimpManager = new MailChimp.MailChimpManager("4886ea5bb9f4255f7c0ae9f8fec52bbf-us20");

            var emailParameter = new EmailParameter
            {
                Email = email
            };
            try
            {
                var result = mailChimpManager.Subscribe("4b1c95ee2b", emailParameter);
                model.Message = "Teşekkürler, Üyeliğiniz başarıyla alındı!";
            }
            catch (MailChimpAPIException e)
            {
                model.Message = e.Message;
            }

            return View("AddSubscribe", model);
        }
    }
}